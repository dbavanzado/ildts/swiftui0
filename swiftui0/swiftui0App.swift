//
//  swiftui0App.swift
//  swiftui0
//
//  Created by Josemaria Carazo Abolafia on 11/5/21.
//

import SwiftUI

@main
struct swiftui0App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
